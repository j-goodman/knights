require 'byebug'
class PolyTreeNode
  attr_reader :children, :parent, :value

  def initialize(value, child_names=[])
    @value = value
    @children = get_children(child_names)
    @parent = nil
  end

  def add_child(child)
    @children << child
    child.parent=self
  end

  def bfs(target_value)
    return self if self.value == target_value
    queue = self.children
    until queue.empty?
      current_val = queue.shift
      # debugger
      if current_val.value == target_value
        return solution = current_val
      else
        queue.concat(current_val.children)
      end
    end
    nil
  end

  def dfs(target_value)
    if self.value == target_value
      return self
    elsif self.children.empty?
      return nil
    else
      self.children.each do |child|
        node = child.dfs(target_value)
        return node unless node.nil?
      end
      return nil
    end
  end

  def get_children(child_names)
    child_nodes = []
    child_names.each do |child_name|
      new_child = PolyTreeNode.new(child_name, [])
      new_child.parent=(self)
      child_nodes << new_child
    end
    child_nodes
  end

  def parent=(new_parent)
    unless @parent.nil?
      @parent.children.delete(self)
    end
    unless new_parent.nil? || new_parent.children.include?(self)
      new_parent.children << self
    end
    @parent = new_parent
  end

  def remove_child(child)
    if self.children.include?(child)
      child.parent= nil
      @children.delete(child)
    else
      raise "That's not my kid."
    end
  end

  def to_s()
    @value
  end
end
