class Knightpath

attr_reader :generated_squares

  MOVES = [[2,1],[-2,1],[2,-1],[-2,-1],[1,2],[1,-2],[-1,2],[-1,-2]]

  def initialize(start_pos, end_pos)
    @start_pos = Node.new(start_pos, nil)
    @end_pos = end_pos
    @generated_squares = []
    @start_pos.generated_squares = @generated_squares
  end


  def build_move_tree(pos = @start_pos)
    if pos == @end_pos
      return pos
    elsif pos.children.empty?
      return nil
    else
      children = pos.find_move
      children.each do |child|
        @generated_squares << child
        if child.position == @end_pos
          return child
        else
          build_move_tree(child)
        end
      end
    end
    nil
  end

  def find_path
    final_node = build_move_tree(@start_pos)
    return nil if final_node == nil
    node = final_node
    ancestors = []
    until node == @start_pos
      ancestors << node
      node = node.parent
    end
    ancestors.reverse
  end

end

class Node
  attr_reader :position
  attr_accessor :parent, :generated_squares

  def initialize(position, parent = nil)
    @position = position
    @children = []
    @parent = parent
    @generated_squares = nil
  end

  def find_move
    all_moves = []
    MOVES.each do |move|
      x, y = self.position
      x += move[0]
      y += move[1]
      all_moves << Node.new([x,y], self) if x.between?(0,8) && y.between?(0,8)
      all_moves.select { |square| !@generated_squares.include?(square) }
      all_moves.each { |square| square.generated_squares = @generated_squares }
    end
    all_moves
  end
end

a = Knightpath.new([0,0], [5,5])
p a.find_path
